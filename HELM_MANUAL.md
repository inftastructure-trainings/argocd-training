# Установить helm
```yml
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
# Добавим helm репозиторий для установки ПО в кластер из helm и обновим его
`helm repo add stable https://charts.helm.sh/stable`
```bash
helm repo update
helm search repo stable
```
# Пример создаия хельм чарта с 

* Проверить как сгенерируется шаблон: `helm template helmchart` (в качестве аргумента папка с чартом)
* Установка чарта `helm install my-chart mychart/`


# Конфигурация многосервисного чарта
### Создать приложение в argocd
```bash
argocd app create multiservice-demo-app \
    --dest-namespace kbadm-space \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/inftastructure-trainings/argocd-training.git \
    --revision main \
    --path multiservice-helmchart/
```