# **В ДОКУМЕНТАЦИИ ЕСТЬ ВСЁ ИСПОЛЬЗОВАТЬ ПОЛЕ ПОИСКА `https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/Template/`**


**https://github.com/argoproj/argo-cd/blob/master/docs/faq.md**

# установка argocd cli
```
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
rm argocd-linux-amd64
```
# установка argocd
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

# вход в UI
форварднуть порт argocd-server:8080

# логин `admin`, пароль находится в секрете argocd-initial-admin-secret (прим. eRzkdV-j4haT2gBR)

# создать технического пользователя
`kubectl get configmap argocd-cm -n argocd -o yaml > argocd-cm.yml`
добавить пользователя в ConfigMap с возможностями входа по ключу и паролю, пример
```yaml
  GNU nano 6.2                                                                                                                    argocd-cm.yml *                                                                                                                           
apiVersion: v1
data:
  accounts.nngle: apiKey,login
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app.kubernetes.io/name":"argocd-cm","app.kubernetes.io/part-of":"argocd"},"name":"argocd-cm","namespace":"argocd"}}
  creationTimestamp: "2022-12-10T15:18:07Z"
  labels:
    app.kubernetes.io/name: argocd-cm
    app.kubernetes.io/part-of: argocd
  name: argocd-cm
  namespace: argocd
  resourceVersion: "465798"
  uid: 50b2886a-6e99-4521-a0b8-e6ea2801fdf7
```
`kubectl apply -f argocd-cm.yml`
# обновим пароль пользователя
```
# if you are managing users as the admin user, <current-user-password> should be the current admin password.
argocd account update-password \
  --account <name> \
  --current-password <current-user-password> \
  --new-password <new-user-password>
```
# обновить роли пользователя
выполнить
`kubectl get configmap argocd-rbac-cm -n argocd -o yaml > argocd-rbac.yml`
добавить поле data в ConfigMap, пример
```yaml
apiVersion: v1
data:
  policy.csv: |
    p, role:org-admin, applications, *, */*, allow
    p, role:org-admin, clusters, get, *, allow
    p, role:org-admin, repositories, get, *, allow
    p, role:org-admin, repositories, create, *, allow
    p, role:org-admin, repositories, update, *, allow
    p, role:org-admin, repositories, delete, *, allow
    p, role:org-admin, projects, get, *, allow
    p, role:org-admin, projects, create, *, allow
    p, role:org-admin, projects, update, *, allow
    p, role:org-admin, projects, delete, *, allow
    g, nngle, role:org-admin
  policy.default: role:''
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app.kubernetes.io/name":"argocd-rbac-cm","app.kubernetes.io/part-of":"argocd"},"name":"argocd-rbac-cm","namespace":"argocd"}}
  creationTimestamp: "2022-12-10T15:18:07Z"
  labels:
    app.kubernetes.io/name: argocd-rbac-cm
    app.kubernetes.io/part-of: argocd
  name: argocd-rbac-cm
  namespace: argocd
  resourceVersion: "465803"
  uid: dd54cd05-859d-47bb-af05-66138ab6a1a2
```

# добавим репозиторий проекта `argocd repo add https://gitlab.com/NickGlebanov/kbm-admin-server`

# войдем в под argocd сервера для использования cli без доп. конфигурации
`kubectl exec -n argocd --stdin --tty argocd-server-757fddb4d7-zlj5q -- /bin/bash`

# создать пример приложения чтобы кластер синхронизировался и можно было создавать приложения из UI
общий пример:
```bash
argocd app create apps \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://github.com/argoproj/argocd-example-apps.git \
    --path ./k8s/*
argocd app sync apps  
```
пример kbadm:
```bash
argocd app create kbadm-release-testapp \
    --dest-namespace kbadm-space \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/NickGlebanov/kbm-admin-server.git \
    --revision release \
    --path k8s/
```
**использовать `--directory-exclude string` для исключения файлов и папок**
тест флага
```bash
argocd app create kbadm-release-testapp \
    --dest-namespace kbadm-space \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/NickGlebanov/kbm-admin-server.git \
    --revision release \
    --path k8s/ \
    --directory-recurse \
# указывается относитльно значения в параметре path, а не относительно корня проекта
    --directory-exclude '{test/*,kbadm-remote-debug.yml}'
```
итоговый манифест тестового приложения
```yaml
project: default
source:
  repoURL: 'https://gitlab.com/NickGlebanov/kbm-admin-server.git'
  path: k8s/
  targetRevision: release
  directory:
    recurse: true
    jsonnet: {}
    exclude: '{test/*,kbadmpod-remote-debug.yml}'
destination:
  server: 'https://kubernetes.default.svc'
  namespace: kbadm-space
```